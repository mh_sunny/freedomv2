<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Widget Plugin 
 * 
 * Install this file as application/plugins/widget_pi.php
 * 
 * @version:     0.1
 * $copyright     Copyright (c) Wiredesignz 2009-03-24
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
class freedommainvideos extends widget
{

    function run($ci_key = "")
    { 
        $CI = &get_instance();
        
        $cache_name = "worldcup/INNER_CONTENT_CACHE_313_4_WC". str_replace(":", "-",  str_replace(".", "-", str_replace("/", "-", base_url()))) . date("Y_m_d");
        if ($s_wcfanpic_content =  $CI->cache->file->get($cache_name) )
        {
            echo $s_wcfanpic_content;
        }
        else
        {
            $obj_post = new Post_model();
            $obj_post_data_fifa_stories = $obj_post->get_posts(array("Inner",'fifa_wordcup'),NULL, NULL, 0, 313, 'smaller', 'post.priority,desc', 4, TRUE, "", "", 0, 1, TRUE);
            $fifa_stories = array(); 
            $i = 0;
            foreach($obj_post_data_fifa_stories as $value)
            {
              $fifa_stories[$i]->id       = $value->id;
              $fifa_stories[$i]->headline = $value->headline;
              $fifa_stories[$i]->content  = $value->content; 
               if($value->lead_material && strpos("http", $value->lead_material)===false)
              {
                $value->lead_material = base_url().$value->lead_material;
              } 
              $fifa_stories[$i]->gallery_images = $value->lead_material;
              $i++;
            } 
            $data['fifa_stories']  = $fifa_stories;
            $this->render($data);
        }
    }  
 
}

