<?php
$CI = & get_instance();

$cache_name = "worldcup/INNER_CONTENT_CACHE_313_4_WC" . str_replace(":", "-", str_replace(".", "-", str_replace("/", "-", base_url()))) . date("Y_m_d");
if ($s_wcfanpic_content = $CI->cache->file->get($cache_name))
{
    echo $s_wcfanpic_content;
}
else
{
    ob_start();
    ?>
 
            <?php
            $i = 0;
			$image_link = '';
            foreach ($fifa_stories as $value):
  				//$image_link = get_images_link($value);
				//src="styles/layouts/tdsfront/freedominair/img/niyazi.png"
 					//$pattern = '%(?:https?://)?(?:www\.)?(?:youtu\.be/| youtube\.com(?:/embed/|/v/|/watch\?v=))([\w-]{10,12})[a-zA-Z0-9\< \>\"]%x';
					//$result = preg_match($pattern, $value->content, $matches); 
					//http://i1.ytimg.com/vi/G0wGs3useV8/sddefault.jpg
					
					if(strlen($value->gallery_images) > 0 ){
 						 $image_link  = $value->gallery_images;
					}else{
						 
						$pattern = '%(?:https?://)?(?:www\.)?(?:youtu\.be/| youtube\.com(?:/embed/|/v/|/watch\?v=))([\w-]{10,12})[a-zA-Z0-9\< \>\"]%x';
						$result = preg_match($pattern, $value->content, $matches); 
						 $image_link =  'http://i1.ytimg.com/vi/'. $matches[1].'/mqdefault.jpg';
					}

					
 				  if ($i >= 1)  break;
				 ?>
                    <div class="col-xs-12 col-md-6  paddingl0 paddingbottom20  text-left">
                         <a title="<?php echo $value->headline ; ?>" href="<?php echo base_url(); ?>freedom-in-the-air-videos/<?php echo $value->id; ?>"><img src="<?php echo $image_link; ?>" class="alignleft img-responsive" alt="" width="94%"></a>
						  <a  href="<?php echo base_url(); ?>freedom-in-the-air-videos/<?php echo $value->id; ?>"><div class="play1"></div> </a>
                 </div> 
			<?php   $i++; ?>
        <?php endforeach; ?>      
     

            <div class=" col-xs-12 col-md-6 paddingl0   text-left">
                <ul id="rarevideosList" class="list-unstyled">
				  <?php
                    $i = 0;
                    $image_link = '';
                    foreach ($fifa_stories as $value):
                         //$image_link = get_images_link($value);
                        //$fifa_stories->gallery_images
                    if ($i > 0): 
                    //http://img.youtube.com/vi/73sWFWqgBg4/mqdefault.jpg
					if(strlen($value->gallery_images) > 0 ){
 						 $image_link  = $value->gallery_images;
					}else{
						 
						$pattern = '%(?:https?://)?(?:www\.)?(?:youtu\.be/| youtube\.com(?:/embed/|/v/|/watch\?v=))([\w-]{10,12})[a-zA-Z0-9\< \>\"]%x';
						$result = preg_match($pattern, $value->content, $matches); 
						 $image_link =  'http://i1.ytimg.com/vi/'. $matches[1].'/mqdefault.jpg';
					}
                      ?>
                         <li>  
                        	<a href="<?php echo base_url(); ?>freedom-in-the-air-videos/<?php echo $value->id; ?>"><img src="<?php echo $image_link; ?>" class="alignleft  img-responsive" alt="" width="144" style="max-height:90px"></a>
							<a href="<?php echo base_url(); ?>freedom-in-the-air-videos/<?php echo $value->id; ?>"><div class ="play2"></div> </a>
                             <h4 class="rarevideos-heading"><a href="<?php echo base_url(); ?>freedom-in-the-air-videos/<?php echo $value->id; ?>"><?php echo strip_tags($value->headline); ?></a></h4>
                            <p class="text-muted">
                               		<?php 
										$postcontent = preg_replace('/<iframe>.*<\/iframe>/is', '', $value->content);
										$postcontent = preg_replace( '@&lt;iframe(?:(?!&gt;).)*&gt;.*?&lt;\/iframe&gt;@i', '',   $postcontent);
										$postcontent = preg_replace('/<iframe.*?\/iframe>/i','', $postcontent);
										echo limit_words($postcontent, 12);
									?>
                                
                                
                             </p> 
                         </li>
                        
 
 				 <?php endif; ?>  
		<?php   $i++; ?>
    <?php endforeach; ?>       
                </ul>  
             </div> <!--//#Right column--> 
    <?php
    $s_wcfanpic_content = ob_get_contents();
    ob_end_clean();

    $CI->cache->file->save($cache_name, $s_wcfanpic_content, 86400);

    echo $s_wcfanpic_content;
}
?>